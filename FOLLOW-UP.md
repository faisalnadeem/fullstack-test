# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?
I used react-bootstrap and moment for frontend implementations. Bootstrap for design and moment for date time formatting.

### Q) What's the command to start the application locally?
   npm start
(Default) `npm start`

---

# General:

### Q) If you had more time, what further improvements or new features would you add?
       I improve the design more and the coding variables means more predictable and understable.

### Q) Which parts are you most proud of? And why?
       Components structure and data manipulation.

### Q) Which parts did you spend the most time with? What did you find most difficult?
       Sped a little bit more time on designing the page.

### Q) How did you find the test overall? Did you have any issues or have difficulties completing?If you have any suggestions on how we can improve the test, we'd love to hear them.
       Overall it was well structured. Provided data file is a good one to explore.
