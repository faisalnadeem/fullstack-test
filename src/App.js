import React from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import ItineraryComponent from './components/ItineraryComponent';

function App() {
  const [itineraries, setItineraries] = React.useState([]);
  const [legs, setLegs] = React.useState([]);

  React.useEffect(() => {
    fetch('flights.json')
      .then((res) => res.json())
      .then((data) => {
        setItineraries(data.itineraries);
        setLegs(data.legs);
      }).catch(error => {
        console.log('error: '+error);
      });
  }, []);


  return (
    <Container style={{backgroundColor: '#e6dfdf'}}>
      <Row>
        {itineraries.map(itinerary => (
          <ItineraryComponent itinerary={itinerary} legs={legs}/>
        ))}
      </Row>
    </Container>
  );
}

export default App;
