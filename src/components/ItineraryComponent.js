import React from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import LegComponent from './LegComponent';

function ItineraryComponent(props){
    const [legs, setLegs] = React.useState([]);

    React.useEffect(()=>{
        let sortedArray = [];
        props.legs.map((first) => {
        sortedArray[props.itinerary.legs.findIndex(def => def === first.id)] = first;
        });
        sortedArray = sortedArray.filter(v => v);
        setLegs(sortedArray);
        console.log(sortedArray)

    }, [props.legs]);

    return(
        <Col md="4" xs="12" style={{marginTop: '30px', marginBottom: '10px'}}>
        <Card>
            <Card.Body style={{padding: '0.85rem'}}>
              <Card.Text>
                  {legs.map(leg => {
                      return(
                         <LegComponent leg={leg}/>
                      );
                  })}
                 <Row><Col md="12" xs="12"><p style={{fontSize: '20px', fontWeight: '600', marginBottom: '0.15rem'}}>{props.itinerary.price}</p></Col></Row>
                 <Row>
                   <Col md="8" xs="8">{props.itinerary.agent}</Col>
                   <Col md="4" xs="4" style={{textAlign: 'right'}}>
                     <Button variant="primary" style={{ fontSize: '20px'}}>SELECT</Button>
                     </Col>
                 </Row>
              </Card.Text>
            </Card.Body>
        </Card>
    </Col>
    );
}

export default ItineraryComponent;