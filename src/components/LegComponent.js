import React from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import moment from 'moment';

function getHoursFromMinutes(a){
    var hours = Math.trunc(a/60);
    var minutes = a % 60;
    var time = hours + ':' +minutes;
    return time;
  }

function Leg(props){
    return(
        <Row>
            <Col md="4" xs="4"><p style={{fontSize: '20px', fontWeight: '600', marginBottom: '0.15rem'}}>{ moment(props.leg.departure_time).format('h:mm')}</p><p style={{color: '#ad9c9c', fontWeight: '600'}}>EDI</p></Col>
            <Col md="4" xs="4"><p style={{fontSize: '20px', fontWeight: '600', marginBottom: '0.15rem'}}>{ moment(props.leg.arrival_time).format('h:mm')}</p><p style={{color: '#ad9c9c', fontWeight: '600'}}>LHR</p></Col>
            <Col md="4" xs="4" style={{textAlign: 'right'}}><p style={{marginBottom: '0.15rem'}}>{getHoursFromMinutes(props.leg.duration_mins)}</p><p style={{color: '#426fd4', fontWeight: '600'}}>{props.leg.stops === 0? 'Direct' : props.leg.stops+' Stops'}</p></Col>
        </Row>
    );
}

export default Leg;